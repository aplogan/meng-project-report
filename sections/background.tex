\section{The Ultrasonic MEMS device}

The ultrasonic MEMS device consists of a set of piezoelectronic transducers arrayed in $N_{trans}$ equally-spaced rows and $N_{trans}$ equally-spaced columns (figure \ref{fig:transducer_array}). The transducers are fabricated with aluminum nitride embedded in a silicon substrate \cite{liu_et_al_2018}. The transducers are all driven to oscillate simultaneously for a short duration at a single frequency in the gigahertz range (e.g. 1.2 GHz), creating an acoustic wave that propagates into the substrate. Each transducer in the array is driven independently, so that they can each oscillate at a different amplitude and phase. When the amplitude at which each transducer is driven corresponds to the intensity of a pixel in a grayscale image, the overall sum of the wave contributions from each transducer forms an ``image" that is conveyed by the intensity of the wave.

\begin{figure}[h]
	\centering
	\includegraphics[width=3 in]{array}
	\caption[Ultrasonic MEMS transducer array schematic]{Ultrasonic MEMS transducer array schematic. The index pairs $(i,j)$ index locations along the $x$ and $y$ axes, so the positive $x$ and $y$ directions are downward and to the right, respectively. These match the axis orientation portrayed later in figure \ref{fig:planes}. Here, $N$ is shorthand for $N_{trans}$.}
	\label{fig:transducer_array}
\end{figure}

Although figure \ref{fig:transducer_array} might suggest that adjacent transducers abut one another, in reality there is a small gap between them. However, this gap was not modeled in the simulation that was the focus of this project. In fact, the simulation assumed a rather simplistic modeling of these transducers. The transducer size was assumed to be $50\ \mu m$ in this work, so that the width of the transducer array is simply $w_{in} = N_{trans}\cdot 50\ \mu m $. 

\section{Fourier Optics Basics}
\label{sec:fourier_optics}

Next I provide sufficient background on Fourier Optics to understand what the simulation does. This material is based on \cite{goodman_introduction_1968}. The physics presented here for optics are not unique to the behavior of light; rather, they are descriptive of waves in general. Therefore, what is presented here applies equally well to acoustic waves in a physical medium such as silicon.

\subsection{The Huygens-Fresnel Principle}

Consider figure \ref{fig:diffrac_geom}, in which monochromatic light emerging through an aperture $\Sigma$ in source plane $z=z_1$ on the left diffracts and is observed at observation plane $z=z_0$ on the right. In figure \ref{fig:diffrac_geom}, $z \equiv z_0 - z_1$, but in the nomenclature of this paper, $z$ refers to the axis running horizontally through the figure, with $z$ increasing from left to right.

\begin{figure}[h]
	\centering
	\includegraphics{diffraction_geometry}
	\caption[Diffraction geometry]{Diffraction geometry. (Figure 4-1 from page 57 of \cite{goodman_introduction_1968})}
	\label{fig:diffrac_geom}
\end{figure}

Denote the complex field amplitude (i.e. amplitude and phase) at point $P_0 = (x_0,y_0,z_0)$ in the observation plane as $\mathbf{U}_0(x_0,y_0)$. Let the light have wavelength $\lambda$ and wavenumber $k= 2\pi / \lambda$. By the Huygens-Fresnel principle, the field at the observation point $P_0$ due to contributions by the source field $\mathbf{U}_1(x_1,y_1)$ at all points $P_1 = (x_1,y_1,z_1)$ in the aperture can be expressed as:

\begin{equation}
	\mathbf{U}_0(x_0,y_0) = \iint\limits_{\Sigma}
	h(x_0,y_0;x_1,y_1) \mathbf{U}_1(x_1,y_1) dx_1 dy_1
	\label{eq:bg1}
\end{equation}

\noindent where

\begin{equation}
	h(x_0,y_0;x_1,y_1) = \frac{ e^{j k r_{0,1}} }{j \lambda r_{0,1}} \cos(\mathbf{\bar{n}},\mathbf{\bar{r}_{01}})
	\label{eq:bg2}
\end{equation}

These are equations 4-1 and 4-2 from \cite{goodman_introduction_1968}. Vector $\mathbf{\bar{r}_{01}}$ is directed from $P_1$ to $P_0$ and has length $r_{0,1} = \sqrt{(x_0-x_1)^2 + (y_0-y_1)^2 + (z_0-z_1)^2}$. The normal vector to the source plane at $P_1$ is denoted as $\mathbf{\bar{n}}$. Therefore,

\begin{equation}
\cos(\mathbf{\bar{n}},\mathbf{\bar{r}_{0,1}}) = \frac{z_0-z_1}{r_{0,1}}
\label{eq:bg3}
\end{equation}

Suppose the aperture is rectangular: $\Sigma = \left\{ (x_1,y_1)\ \vert\ x_1 \in [x_{1,min},x_{1,max}], y_1 \in [y_{1,min},y_{1,max}] \right\}$. Combining this with equations \ref{eq:bg1}, \ref{eq:bg2}, and \ref{eq:bg3}, we have

\begin{equation}
	\mathbf{U}_0(x_0,y_0) = \frac{z_0-z_1}{j \lambda}
	\int\limits_{y_{1,min}}^{y_{1,max}}
	\int\limits_{x_{1,min}}^{x_{1,max}}
	\frac{ e^{j k r_{0,1}} }{r_{0,1}^2}
	\mathbf{U}_1(x_1,y_1) dx_1 dy_1
	\label{eq:bg4}
\end{equation}

\subsection{Converging Lenses and the Fourier Transform}

Next consider figure \ref{fig:converging_lens}, which portrays a \emph{converging lens}, and in particular, a \emph{biconvex lens}. Such a lens is characterized by its focal length, $f$, a positive number. As shown in figure \ref{fig:converging_lens}, the effect of the converging lens on parallel incident rays of light coming from the left is to focus them to a point $f$ beyond the center of the lens, along the $z$-axis in our nomenclature.

\begin{figure}[h]
	\centering
	\includegraphics{converging_lens}
	\caption[Converging lens geometry]{Converging lens geometry. (Figure 5-4 from page 82 of \cite{goodman_introduction_1968})}
	\label{fig:converging_lens}
\end{figure}

Now consider figure \ref{fig:object_against_lens}, which portrays an object against the left side of the lens. Such an ``object" could be an incident wavefront, even one computed via equation \ref{eq:bg4}. Note that we regard as negligible the gap between the object and lens that increases from the center of the lens to its outer edges.

\begin{figure}[h]
	\centering
	\includegraphics{object_against_lens}
	\caption[Object against a converging lens]{Object against a converging lens. (Figure 5-5a from page 84 of \cite{goodman_introduction_1968})}
	\label{fig:object_against_lens}
\end{figure}

The complex field amplitude on the right side of the lens, $\mathbf{U}'_l(x,y)$, due to the field on the left side (which is our ``object" in figure \ref{fig:object_against_lens}), $\mathbf{U}_l(x,y)$, is given by:

\begin{equation}
\mathbf{U}'_l(x,y) = \mathbf{U}_l(x,y)P(x,y)\exp\left(-j\frac{k}{2f}\left(x^2+y^2\right)\right)
\label{eq:bg5}
\end{equation}

\noindent where $P(x,y)$ is called the \emph{pupil function} and is given by

\begin{equation}
P(x,y) =
\left\{
\begin{array}{cl}
1, & \text{inside the lens aperture} \\
0, & \text{otherwise}
\end{array}
\right.
\label{eq:bg6}
\end{equation}

This corresponds to equation 5-12 in \cite{goodman_introduction_1968}.

Note that equation \ref{eq:bg5} is a radial function, with $\rho^2 = x^2 + y^2$ being the square of the distance from the center of the lens.\footnote{It is tempting to use $r$ for this radial coordinate, but recognize we've already been using $r$ to denote inter-plane distances. We make exception, however, for specific radius constants, such as lens radius, $r_{len}$.} In particular, the following gives the phase transformation of the lens within its aperture (that is, for all $(x,y)$ for which $P(x,y)=1$):

\begin{equation}
	\begin{split}
	\phi_{lens}(x,y)  &= -\frac{k}{2f}\left(x^2+y^2\right) \\
	\phi_{lens}(\rho) &= -\frac{k}{2f}\rho^2
	\end{split}
	\label{eq:bg7}
\end{equation}

Finally, consider figure \ref{fig:object_in_front_of_lens}, which portrays an object at some distance $d_o$ to the left of the lens. In particular, we are concerned with the special case in which the object is placed at a distance $d_o = f$ to the left of the lens.

\begin{figure}[h]
	\centering
	\includegraphics{object_in_front_of_lens}
	\caption[Object in front of a converging lens]{Object in front of a converging lens; we are concerned with the special case of $d_o = f$. (Figure 5-5b from page 84 of \cite{goodman_introduction_1968})}
	\label{fig:object_in_front_of_lens}
\end{figure}

The field at $f$ to the right of the lens plane, $\mathbf{U}_f(x_f,y_f)$ due to the field at $f$ to the left of the lens plane (the ``object" in figure \ref{fig:object_in_front_of_lens}), is given by

\begin{equation}
\mathbf{U}_f(x_f,y_f) =
\frac{ 1 }{ j \lambda f }
\mathbf{F}_o \left( \frac{x_f}{\lambda f}, \frac{y_f}{\lambda f} \right)
\label{eq:bg8}
\end{equation}

\noindent where $\mathbf{F}_o(f_x,f_y)$ is the Fourier transform of the object field to the left of the lens, $\mathbf{U}_o(x_o,y_o)$ that is,

\begin{equation}
	\mathbf{F}_o(f_x,f_y) =
	\iint\limits_{-\infty}^{\infty}
	\mathbf{U}_o(x_o,y_o)
	e ^ { -j2\pi (f_x x_o + f_y y_o) }
	dx_o dy_o
	\label{eq:bg9}
\end{equation}

Equation \ref{eq:bg8} tells us that the field at $f$ to the right of the lens relates to the Fourier transform of the field at $f$ to the left of the lens,
\begin{itemize}
	\item evaluated at spatial frequencies $f_x = x_f/\lambda f$ and $f_y = y_f/\lambda f$,
	\item with magnitude scaled by $1 / \lambda f$, and
	\item with a phase shift of $-\pi/2$ (from the $1/j$ factor).
\end{itemize}

Equation \ref{eq:bg8} corresponds to equation 5-19 in \cite{goodman_introduction_1968} with $d_o = f$, and is \emph{an approximation} whose accuracy relies on a few assumptions. First, the derivation assumes that $z$-axis distances between the planes of interest ($f$ in this case) are large compared to maximum object extents along the $x$- and $y$-axes, so that $\cos(\mathbf{\bar{n}},\mathbf{\bar{r}_{0,1}}) \approx 1$ in equation \ref{eq:bg3}. This assumption implies point-to-point distances $r_{0,1}$ approximately equal plane-to-plane distance $z_0 - z_1$ in figure \ref{fig:diffrac_geom}. Second, it assumes the lens is large enough that $P(x,y) = 1$ throughout the area of interest in the $x$- and $y$-axes.

\section{Prior Work: The Simulation}
\label{sec:sim}

June Ho Hwang\footnote{A Cornell PhD candidate working in the \href{https://sonicmems.ece.cornell.edu}{Cornell SonicMEMS Laboratory}} wrote a C++ simulation of the wave physics of the Ultrasonic MEMS device specifically for the setup portrayed in figure \ref{fig:planes}. Specifically, an array of ultrasonic MEMS transducers (as in figure \ref{fig:transducer_array}) at $z=0$ creates a wavefront according to some input grayscale image. Each transducer replicates the amplitude information in one pixel of the input image, oscillating at a fixed frequency for a short time to send a wave into the silicon substrate, in the positive-$z$ direction. This input wave contains amplitude information only, corresponding to the capabilities of the Ultrasonic MEMS device at the time the simulation was written. However, at the time of this writing, the ability to for each transducer to have independent phase and amplitude is the subject of ongoing research. The simulation then computes the propagation of the wave in the positive-$z$ direction, through the silicon substrate and a pair of lenses, as described by equations \ref{eq:bg4} and \ref{eq:bg5}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{planes}
	\caption[Canonical simulation setup]{Canonical simulation setup for computation of Fourier and inverse Fourier transforms, with or without filtering. Planes are numbered left to right, starting from the transducer array (plane 0). Note the $z$-axis (in blue, here), runs through the center of each plane. The $x$- and $y$-axes are oriented to match the indexing scheme of figure \ref{fig:transducer_array}, and together with the $z$-axis form a right-handed coordinate system. The resolution of the transducer array -- that is, the number of transducers in the array, $N_{trans}\times N_{trans}$ -- is arbitrary. In the simulation it matches the resolution of the input image, prior to its upsampling.}
	\label{fig:planes}
\end{figure}

In what follows, ``wave" will refer to the complex field amplitude as a function of $x$ and $y$ ($\mathbf{U}(x,y)$ in the above), computed by the simulation for a fixed value of $z$.

As shown in figure \ref{fig:planes}, two lenses of the same positive focal length $f$ are placed at $z=f$ and $z=3f$ from the transducer array. The simulation computes the wave on the left side of each lens according to equation \ref{eq:bg4}, and then immediately applies a phase transformation according to equation \ref{eq:bg5} to compute the wave on the right side of each lens. In the simulation, the lenses are ideal and have zero thickness along the $z$-axis, so the ``left" and ``right" sides of a lens have the same $z$ coordinate, but merely refer to the input and output of equation \ref{eq:bg5}. The lenses also have infinite radius (i.e. $P(x,y)=1\ \forall\ (x,y)$ in equation \ref{eq:bg5}), although they are constrained by the output image width $w_{out}$, which we'll discuss further in a moment. The wave can be computed at arbitrary positions along the $z$-axis by equation \ref{eq:bg4}, where the source wave for equation \ref{eq:bg4} is:
\begin{itemize}
	\item the wave at $z=0$, for all observation locations $z\in(0,f)$
	\item the wave just to the right of the first lens ($z=f^+$), for all observation locations $z\in(f,3f)$
	\item the wave just to the right of the second lens($z=3f^+$), for all observation locations $z\in(3f,\infty)$
\end{itemize}

In figure \ref{fig:planes}, the Fourier transform of the input image is formed at $z=2f$, and the inverse Fourier transform of the Fourier transform (the input image, upside down) is formed at $z=4f$. For convenience, this paper will henceforth refer to the planes in the canonical simulation setup of figure \ref{fig:planes} as planes 0 through 4, according to the number of focal lengths from the input plane.

It's important to recognize that equations \ref{eq:bg4} and \ref{eq:bg5} are exact -- their derivations do not entail approximations. Therefore, simulation error will come not from the use of these equations, but rather from numerical approximations thereof. The two  primary sources of numerical error in the simulation is in computing equation \ref{eq:bg4}, which is approximated by the midpoint rule and is constrained to finite dimensions along the $x$ and $y$ axes:

\begin{equation}
	\begin{split}
		\mathbf{U}_0(i_0,j_0) =
		\frac{z_0-z_1}{\lambda}
		\sum\limits_{i_1=0}^{N_{x_1}-1}
		\sum\limits_{j_1=0}^{N_{y_1}-1}
		\frac{e^{jkr_{0,1}}}{r_{0,1}^2} \mathbf{U}_1(i_1,j_1)
		\Delta x_1 \Delta y_1
		\\
		\forall\ \left\{ (i_0,j_0)\ \vert\ 
		                 i_0 \in \{0,\dots,N_{x_0}\},\ 
		                 j_0 \in \{0,\dots,N_{y_0}\}
		         \right\}
	\end{split}
	\label{eq:bg10}
\end{equation}

\noindent where

\begin{equation}
	\left\{\ 
	\begin{aligned}
		\mathbf{U}_0(i_0,j_0) &\equiv \mathbf{U}_0 \left( x_0(i_0),y_0(j_0) \right) \\
		\mathbf{U}_1(i_1,j_1) &\equiv \mathbf{U}_1 \left( x_1(i_1),y_1(j_1) \right)
	\end{aligned}
	\right.
	\label{eq:bg11}
\end{equation}

\begin{equation}
	\left\{\ 
	\begin{aligned}
		x_0(i_0) &\coloneqq x_{0,min} + i_0 \Delta x_0 \\
		y_0(j_0) &\coloneqq y_{0,min} + j_0 \Delta y_0 \\
		x_1(i_1) &\coloneqq x_{1,min} + (i_1 + 1/2) \Delta x_1 \\
		y_1(j_1) &\coloneqq y_{1,min} + (j_1 + 1/2) \Delta y_1
	\end{aligned}
	\right.
	\label{eq:bg12}
\end{equation}

\begin{equation}
	\left\{\ 
	\begin{aligned}
		\Delta x_0 &\coloneqq \left( x_{0,max} - x_{0,min} \right) /  N_{x_0} \\
		\Delta y_0 &\coloneqq \left( y_{0,max} - y_{0,min} \right) /  N_{y_0} \\
		\Delta x_1 &\coloneqq \left( x_{1,max} - x_{1,min} \right) /  N_{x_1} \\
		\Delta y_1 &\coloneqq \left( y_{1,max} - y_{1,min} \right) /  N_{y_1}
	\end{aligned}
	\right.
	\label{eq:bg13}
\end{equation}

\begin{equation}
	\begin{split}
	r_{0,1}
	&= r_{0,1}\left(x_0(i_0),y_0(j_0),z_0,x_1(i_1),y_1(j_1),z_1\right) \\
	&= \sqrt{ \left(x_0(i_0)-x_1(i_1)\right)^2 + \left(y_0(j_0)-y_1(j_1)\right)^2 + (z_0-z_1)^2}
	\end{split}
	\label{eq:bg14}
\end{equation}

Note that equation \ref{eq:bg10} drops the constant $1/j$ factor from equation \ref{eq:bg4}. Equations \ref{eq:bg9} through \ref{eq:bg13} are left in their most general form, whereas in reality, the simulation imposes the following symmetries:
\begin{equation}
	\left\{\ 
	\begin{aligned}
		-x_{0,min}=x_{0,max}=-y_{0,min}=y_{0,max} \\
		-x_{1,min}=x_{1,max}=-y_{1,min}=y_{1,max} \\
		N_{x_0}=N_{y_0} \eqqcolon N_{0}\\
		N_{x_1}=N_{y_1} \eqqcolon N_{1}
	\end{aligned}
	\right.
	\label{eq:bg15}
\end{equation}

\noindent which further imply

\begin{equation}
	\left\{\ 
	\begin{aligned}
		\Delta x_0 = \Delta y_0 \\
		\Delta x_1 = \Delta y_1
	\end{aligned}
	\right.
\end{equation}

The numerical integration in equation \ref{eq:bg10} integrates over $N_1 \times N_1$ points in the source plane for each of $(N_0+1) \times (N_0+1)$ points in the observation plane. Moreover, as equation \ref{eq:bg12} indicates, the spacing of points $(x_1(i_1),y_1(j_1))$ in the source plane follows the midpoint integration rule, while in the observation plane, points $(x_0(i_0),y_0(j_0))$ are located on $(x,y)$ grid intersection points. The benefit of this approach is that when the symmetries of equation \ref{eq:bg15} are satisfied and $N_0$ is even, there will be an observation point on the z-axis, i.e., $\left(x_0(N_0/2),y_0(N_0/2)\right) = (0,0)$. However, in the simulation, the observation plane on the left side of a lens becomes the source plane in a subsequent application of equation \ref{eq:bg4} on the right side of the lens, but there is an inherent mismatch between the $(x,y)$ grids on which the two planes are represented. In the original version of the simulation, $N_0=N_1=100$. In particular, when the wave is computed on a $101\times101$ grid at the left side of a lens and subsequently transformed by the lens to be used as the source field for the next computation, only the first $100\times100$ wave values are kept as the new source wave. Moreover, although these values were computed at $(x,y)$ grid intersection points, they are then taken to be the wave values at $(x,y)$ grid midpoints. This implicit shift introduces additional numerical error. This shortcoming was addressed in the simulation rewrite, as described in chapter \ref{chapter:improve}.

An important question that arises is how does the run-time of the simulation scale with image resolution? For each source-observation plane pair in which equation \ref{eq:bg10} is computed, $(N_0+1)^2$ values in the observation plane must be computed each by integrating over $N_1^2$ points in the source plane, giving $\mathcal{O}\left(N_1^2 (N_0+1)^2\right) = \mathcal{O}\left(N_1^2 N_0^2\right)$ performance\footnote{Recall that big-O arithmetic considers only the contribution of the run-time dominant component. Although strictly speaking, $\mathcal{O}\left(N_1^2 (N_0+1)^2\right) = \mathcal{O}\left(N_1^2N_0^2 + N_1^2\right)$, the contribution of the quartic component $N_1^2N_0^2$ dominates over the quadratic component $N_1^2$, and the latter is dropped from the final expression.}. Let the plane 0 image have resolution $N_{in} \times N_{in}$, and let planes 1 through 4 have resolution $N_{out} \times N_{out}$. For the application of equation \ref{eq:bg10} from plane 0 to plane 1, we can see the run-time scales as $\mathcal{O}\left(N_{in}^2 N_{out}^2\right)$. For all subsequent plane pairs at which equation \ref{eq:bg10} is calculated (plane 1 to plane 2, plane 1 to plane 3, and plane 3 to plane 4), the run-time scales as $\mathcal{O}\left(N_{out}^4\right)$. Adding this all up, the total run-time scales as $\mathcal{O}\left((N_{in}^2+3N_{out}^2)N_{out}^2\right)$. Note that the lens transformation computations are left out of this estimate as they have a negligible contribution to overall run-time.

Consider, then, the impact of doubling image resolution on the simulation run-time. That is, let $N_{in}' = 2N_{in}$ and $N_{out}' = 2N_{out}$, so that each image is twice as wide and twice as tall, with four times as many pixels each. The run-time now scales as $\mathcal{O}\left((N_{in}'^2+3N_{out}'^2)N_{out}'^2\right)
=
\mathcal{O}\left(\left((2N_{in})^2+3(2N_{out})^2\right)(2N_{out})^2\right)
=
16\mathcal{O}\left((N_{in}^2+3N_{out}^2)N_{out}^2\right)$. So in general, scaling the input and output image resolution by $\alpha$ along both the $x$ and $y$ axes scales the simulation run-time by $\alpha^4$. This turned out to be a major limitation in running the simulation for interesting image resolutions, and profoundly impacted my experience of this project.

The original simulation presented a few alternatives for simulating lenses. First, the simulation implemented the ideal lens equation (equation \ref{eq:bg5}) directly. However, a lens that would perfectly present such a phase transformation to ultrasonic waves is basically impossible to realize with physical structures built in silicon substrate. So the simulation also included options for simulating physically realizable lens structures. Each of these options still relied on the basic premise of a radially-dependent phase shift (as per equation \ref{eq:bg7}) and did not simulate any loss/attenuation, lens thickness, or other physical effects. One approach first quantized $x$ and $y$ into 20 equally spaced bins each across the width of the lens plane ($w_{out}$ in figure \ref{fig:planes}), and then plugged those quantized $x$ and $y$ coordinates into equation \ref{eq:bg5}. Two other approaches performed the quantization along the radial axis, and relied on a look-up table of precomputed phase shifts for different radii. One approach used the radius $\rho$ for the lookup (figure \ref{fig:lens_data}) while the other used $\rho^2$ for the lookup. The details of how the phase was computed in this approach, how the radial coordinate was quantized, or the rationale for performing a lookup with $\rho^2$, are not understood by the author.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{lens_data}
	\caption[Precomputed lens phase as a function of radial coordinate]{Precomputed lens phase $\phi_{lens}$ as a function of radial coordinate $\rho$. This particular mapping, found in the original simulation, is for $\rho_{max} \approx 502\ \mu m$.}
	\label{fig:lens_data}
\end{figure}

A feature of the original simulation which later proved to be of utmost importance for its accuracy -- a point which we'll consider further in chapter \ref{chapter:fourier_transform} -- is that it upsampled the input image before using it as the plane 0 source wave in equation \ref{eq:bg10}. Here, ``upsampling" means simply repeating the value of each pixel some number of times along the $x$ and $y$ axes, so that each pixel is replicated throughout a square region in the resulting image. We are \emph{not} referring to the digital signal processing technique which uses an upsampling filter to increase the sample rate while rejecting high-frequency images. The original simulation used an upsampling factor of 5, so that, for example, a $20\times20$ pixel image (representing transducers arrayed in 20 rows and 20 columns, equally spaced) would be transformed to a $100\times100$ image. (Actually, the simulation didn't explicitly upsample the image in memory -- it simply accomplished this by appropriately indexing the input image in its computations. But the effect was the same.) Because the input and output images of this upsampling step have the same physical dimensions, the effect of upsampling is to increase the spatial sampling rate, or equivalently, to decrease the size of each pixel. The effect also models a constant wave amplitude across the face of each transducer, with no ``smoothing" of the wave from one transducer to the next. This effect is quite apparent in figure \ref{fig:init_sim_a}.

The original simulation was hard-coded with the parameters given in table \ref{table:bg1}. A sample result of running the simulation with these parameters is given in figure \ref{fig:initial_sim_output}.

\begin{table}[h]
	\centering
	\begin{tabular}{|p{0.4\textwidth}|p{0.4\textwidth}|}
		\hline
		\textbf{Parameter} & \textbf{Value} \\
		\hline
		Input image resolution & $20 \times 20$ pixels \\
		\hspace{1em}(before upsampling) & \\
		\hspace{1em}($N_{trans} \times N_{trans}$) & \\
		Input image upsampling & $5\times$ \\
		Input resolution ($N_{in} \times N_{in}$) & $100 \times 100$ pixels \\
		\hspace{1em}(after upsampling) & \\
		Input width ($w_{in}$) & $1000\ \mu m$ \\
		Transducer size & $(1000\ \mu m) / 20 = 50\ \mu m$ \\
		Input pixel size & $(1000\ \mu m) / 100 = 10\ \mu m$ \\
		Wavelength & $(8433\ m/s) / (1.2\ GHz) = 7.0275\ \mu m$ \\
		Input pixels per wavelength & $(7.0275\ \mu m) / (10\ \mu m) = 0.70275$ \\
		Output width ($w_{out}$) & $1000\ \mu m$ \\
		Output resolution & $101 \times 101$ pixels \\
		\hspace{1em}($(N_{out}+1) \times (N_{out}+1)$) & \\
		Output pixel size & $(1000\ \mu m) / 100 = 10\ \mu m$ \\
		Output pixels per wavelength & $(7.0275\ \mu m) / (10\ \mu m) = 0.70275$ \\
		Lens focal length ($f$) & $1000\ \mu m$ \\
		Lens radius ($r_{lens}$) & $\infty$ \\
		Planes 1-4 $z$-axis locations & $1000,2000,3000,4000\ \mu m$ \\
		Other output $z$-axis location & $10\ \mu m$ \\
		\hline
	\end{tabular}
	\caption{Hard-coded parameters for the simulation as it was received by the author in December 2019}
	\label{table:bg1}
\end{table}

\begin{figure}[h]
	\centering
	\subfloat[Input image (transduced wave)]{\label{fig:init_sim_a}\includegraphics[width=0.32\textwidth]{a_0000_um_upsamp_5}}\hfill
	\subfloat[Wave at $z = 10\ \mu m$]{\label{fig:init_sim_b}\includegraphics[width=0.32\textwidth]{a_0010_um_gray}}\hfill
	\subfloat[Wave at plane 1]{\label{fig:init_sim_c}\includegraphics[width=0.32\textwidth]{a_1000_um_gray}}\\
	\subfloat[Wave at plane 2]{\label{fig:init_sim_d}\includegraphics[width=0.32\textwidth]{a_2000_um_gray}}\hfill
	\subfloat[Wave at plane 3]{\label{fig:init_sim_e}\includegraphics[width=0.32\textwidth]{a_3000_um_gray}}\hfill
	\subfloat[Wave at plane 4]{\label{fig:init_sim_f}\includegraphics[width=0.32\textwidth]{a_4000_um_gray}}\\
	\caption[Sample input and output for the simulation using its initial parameters]{Sample input and output for the simulation using its initial parameters (those given in table \ref{table:bg1})}
	\label{fig:initial_sim_output}
\end{figure}

\section{A Note About Simulation Output Images}
It is important to keep in mind that throughout this paper, the simulation output images, such as those shown in figure \ref{fig:initial_sim_output}, have been mapped from complex wave values to grayscale images. Phase information is lost in this mapping and is therefore not portrayed. Furthermore, in order to  provide the greatest amount of display dynamic range for a given image, black (pixel value 0) and white (pixel value $255=2^8-1$) represent the minimum and maximum amplitudes in the complex wave depicted, respectively, with amplitudes between color-mapped on a linear, not logarithmic scale. This mapping is generally different from one image to the next, and can prove misleading when two similar images are viewed side-by-side. Figures \ref{fig:edge_detect_results:b} and \ref {fig:edge_detect_results:c} provide a good example of this effect.
