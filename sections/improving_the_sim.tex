\label{chapter:improve}

\section{Rewrite}

The original simulation's C++ implementation did not make use of the C++ Standard Template Library's \texttt{std::complex} type,\footnote{\texttt{std::complex} is a type-generic class with templated routines for compiler-optimized complex number arithmetic (\url{https://en.cppreference.com/w/cpp/numeric/complex})} but instead explicitly stored and computed real and imaginary parts of numbers separately. For example, the complex integrand of equation \ref{eq:bg10} was computed explicitly as follows\footnote{Ignoring the real factor $(z_0-z_1) \Delta x_1 \Delta y_1 / (\lambda r_{0,1}^2)$}:

\begin{equation}
	\begin{split}
		e^{jkr_{0,1}} \mathbf{U}_1
		=
		\left[ \cos(kr_{0,1}) + j \sin(kr_{0,1}) \right]
		\left[ \mbox{Re}\left\{\mathbf{U}_1\right\} + j \mbox{Im}\left\{\mathbf{U}_1\right\} \right]
		\\
		\rightarrow\left\{\ 
		\begin{split}
			\mbox{Re}\left\{e^{jkr_{0,1}} \mathbf{U}_1\right\}
			&= \left[ \cos(kr_{0,1}) \mbox{Re}\left\{\mathbf{U}_1\right\} - \sin(kr_{0,1}) \mbox{Im}\left\{\mathbf{U}_1\right\} \right]
			\\
			\mbox{Im}\left\{e^{jkr_{0,1}} \mathbf{U}_1\right\}
			&= \left[ \cos(kr_{0,1}) \mbox{Im}\left\{\mathbf{U}_1\right\} + \sin(kr_{0,1}) \mbox{Re}\left\{\mathbf{U}_1\right\} \right]
		\end{split}
		\right.
	\end{split}
	\label{eq:cplx_integrand}
\end{equation}

\section{Speed Up}
\label{sec:speed_up}

What worked, what didn't work?

\begin{equation}
	\begin{split}
		&  r_{0,1}\left(x_0(i_0+1),y_0(j_0+1),z_0,x_1(i_1+1),y_1(j_1+1),z_1\right) \\
		&= r_{0,1}\left(x_0(i_0)+\Delta x_0,y_0(j_0)+\Delta y_0,z_0,x_1(i_1)+\Delta x_1,y_1(j_1)+\Delta y_1,z_1\right) \\
		&= \sqrt{ \left(x_0(i_0)-x_1(i_1)+\Delta x_0-\Delta x_1\right)^2 + \left(y_0(j_0)-y_1(j_1)+\Delta y_0-\Delta y_1\right)^2 + (z_0-z_1)^2} \\
		&\overset{\Delta x_0=\Delta x_1,\ \Delta y_0=\Delta y_1}{=} \sqrt{ \left(x_0(i_0)-x_1(i_1)\right)^2 + \left(y_0(j_0)-y_1(j_1)\right)^2 + (z_0-z_1)^2} \\
		&= r_{0,1}\left(x_0(i_0),y_0(j_0),z_0,x_1(i_1),y_1(j_1),z_1\right) \\
		&\eqqcolon \tilde{r}_{0,1,\Delta x,\Delta y,\Delta z}(i_0-i_1,j_0-j_1) \\
		&\overset{x_{0,min}=x_{1,min},\ y_{0,min}=y_{1,min}}{=} \tilde{r}_{0,1,\Delta x,\Delta y,\Delta z}(|i_0-i_1|,|j_0-j_1|) \\
	\end{split}
	\label{eq:inter_pt_dist}
\end{equation}

\noindent where $\Delta x$ and $\Delta y$ are the integration lattice spacing in both the source and observation planes and $\Delta z \coloneqq z_0-z_1$ is the plane separation. Notice that in the simulation implementation, $\Delta x = \Delta y$, but equation \ref{eq:inter_pt_dist} leaves the expression in general form. Thus we see that when the source and observation planes use the same integration lattice spacing, the distance between points in the two planes becomes a function of the absolute differences between the lattice coordinates.

\section{New Features}

Note that the current upsampling step has two equivalent representations in MATLAB. Given a matrix \texttt{A} and an upsampling factor \texttt{u}, the following two approaches yield the same result:

\begin{verbatim}
% First approach
p = floor(u/2)
A_upsampled = conv2(upsample(upsample(A,u,p)',u,p)', ones(u), 'same')
% Second approach
A_upsampled = kron(A, ones(u))
\end{verbatim}

\begin{equation}
	\rho_{quantized,L} =
	\left\{
	\begin{array}{ll}
	\frac{\rho_{max}}{L} \left( \left\lfloor \frac{L}{\rho_{max}}\rho \right\rfloor + \frac{1}{2} \right) & \text{\hspace{1em}(mid-rise)} \\
	\frac{\rho_{max}}{L} \left\lfloor \frac{L}{\rho_{max}}\rho + \frac{1}{2} \right\rfloor                & \text{\hspace{1em}(mid-tread)}
	\end{array}
	\right.
\end{equation}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{radius_quantization}
	\caption[Quantization of lens radial coordinate]{Quantization of lens radial coordinate for $L=10$ quantization levels}
	\label{fig:radius_quantization}
\end{figure}

\section{Doxygen}

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{doxygen_function}
	\caption[Sample of Doxygen-generated HTML documentation for a C++ function]{Sample of Doxygen-generated HTML documentation for a C++ function in the simulation code}
	\label{fig:doxygen_function}
\end{figure}

\begin{figure}[p]
	\centering
	\includegraphics[width=\textwidth]{call_graph}
	\caption[Doxygen-generated function call graph]{Doxygen-generated function call graph for every function in the simulation code (excluding library functions)}
	\label{fig:call_graph}
\end{figure}