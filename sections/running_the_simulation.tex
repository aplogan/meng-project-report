\label{appendix:run_sim}

The simulation uses a text-based interface and must be run from a command shell (e.g. Windows Command Prompt). The simulation has many different options, a few of which are mandatory in order to do anything interesting. Simply run the executable without any input arguments or with the \texttt{--help} option to see a listing of all the available options with descriptions; that listing is given in section \ref{sec:option_listing}. Greater detail on using these options is given here.

\section{Input}
The input to the simulation is one of the following:
\begin{itemize}
	\item An image (\texttt{--inimg}) containing, implicitly, real data for the input wave at the input plane (typically thought of as the transducer array). The image must be square. The simulation will convert color images to grayscale (using a codec-defined mapping, depending on the format of the input image), optionally invert the grayscale (\texttt{--img-inv}), optionally apply a grayscale threshold (set by \texttt{--img-thresh}) to obtain a black-and-white image (if \texttt{--img-bw} is set), and finally, optionally upsample by an integer amount specified by \texttt{--img-upsamp}. The size of the portion of the input plane given by the image data may be varied with \texttt{--inwidth}. Note that this width remains unchanged by upsampling; that is, the effect of upsampling the input image is to reduce the size of input pixels.

	\item A CSV file (\texttt{--incsv}) containing data for the input wave at the input plane (typically thought of as the transducer array). The data may be real or complex valued. The simulation expects complex numbers to be formatted as \texttt{<real part>$\pm$<imaginary part>i}. Except for newlines, whitespace is ignored. The CSV file must specify a square array. That is, the file should have N lines of data, each with N comma-separated entries. The size of the portion of the input plane given by the CSV file data may be varied with \texttt{--inwidth}. As with images, data from CSV files may be upsampled by an integer amount specified by \texttt{--img-upsamp}. Again, the width specified by \texttt{--inwidth} remains unchanged by upsampling; that is, the effect of upsampling the input image is to reduce the size of input pixels.
\end{itemize}

Information about the input file (for example, its resolution or size, whether an input image is grayscale, or whether a CSV specifies real-valued data) by using the \texttt{--ininfo} option. The program will exit after displaying the information.

\section{Output}
Output of the simulation corresponds to the wavefront as calculated in square slices along the $z$-axis, parallel to the input plane. These $z$-axis snapshot locations may be arbitrarily specified in microns from the input plane with the \texttt{--oloc} option. (Negative $z$-axis offsets are not supported.) The argument to the option may be a comma-separated list, and may employ MATLAB range notation. For example, \texttt{--oloc 2:2:20,1000:100:4000} requests snapshot locations every 2 microns from the input plane up through 20 microns, and from 1000 microns to 4000 microns, with 100 micron steps. Except for when requesting Fourier transform output (more on this later), the size (physical dimensions) and resolution of these snapshots default to match those of the input, but may be set with \texttt{--owidth} and \texttt{--ores}. If \texttt{--ores} is specified but \texttt{--owidth} is not, \texttt{--owidth} defaults to a value that preserves pixel size from intput to output.

The output files may be written in any of three formats:

\begin{itemize}
	\item Grayscale images in PNG format (\texttt{--ogray}). These images display the computed wavefront \emph{amplitude only}, with the minimum amplitude (not necessarily 0) mapped to grayscale minimum (black) and and the maximum amplitude mapped to grayscale maximum (white). Note that the mapping of amplitude to grayscale generally differs from one image to the next. I experimented with fixing the mapping across all images, and found that outlier amplitudes in a few images could cause the remaining images to appear washed out. The grayscale image filenames will end in \texttt{\_gray.png}.

	\item Color images in PNG format (\texttt{--ocolor}). These images are obtained by applying a color map (MATLAB’s ``jet" map, to be specific) to the grayscale image. (Grayscale images needn’t be written out in order to get color images). The color image filenames will end in \texttt{\_color.png}.

	\item CSV files containing wavefront complex values (\texttt{--ocsv}). The formatting of the CSV files will be as described for the \texttt{--incsv} option. This means that files created by the \texttt{--ocsv} option may also be subsequently used as input with the \texttt{--incsv} option. These CSV files can be imported to MATLAB with the \texttt{readmatrix} function for follow-on processing.
\end{itemize}

Here is the output file naming scheme. Suppose the input file is called \texttt{some/path/input\_file.png} (for \texttt{--inimg}) or \texttt{some/path/input\_file.csv} (for \texttt{--incsv}). Then all output file names will begin with \texttt{input\_file}. After that, the $z$-axis location of the image is given in microns (e.g. \texttt{\_1000\_um}) with the minimum number of digits specified by \texttt{--odigit}. Manipulations applied to the input in order to prepare for the simulation (e.g. \texttt{--inupsamp}, \texttt{--img-inv}, or \texttt{--img-bw}) result in output files using $z$-axis location 0 along with a descriptive suffix (e.g. \texttt{\_0000\_um\_inv} or \texttt{\_0000\_um\_bw}). When the simulation is running, whenever a Fourier transform is extracted or a lens or filter transformation is applied, if output was requested at that location, then two files of each type requested are output: the first, prior to the transformation, the second, after. The latter includes a \texttt{\_fourier\_transform}, \texttt{\_post\_lens}, or \texttt{\_post\_filt} suffix to indicate the kind of transformation. Finally, an appropriate suffix is added according to the type of output: \texttt{\_color.png} for \texttt{--ocolor}, \texttt{\_gray.png} for \texttt{--ogray}, and \texttt{.csv} for \texttt{--ocsv}.

Output files are placed in a subdirectory of the current directory (from which the simulation is run) called \texttt{sim\_output/YYYYMMDD\_HHMMSS}, where the date and time are filled in according to when the simulation is executed. The default output directory can be overridden with the \texttt{--odir} option.

The output directory will always include \texttt{command\_line\_\#\#.txt} and \texttt{log\_\#\#.txt} files giving, respectively, the command line used to run the simulation (which can be used as a parameter file to re-run the simulation with reproducible results again -- more on this later), and a copy of all text that is printed to the terminal window throughout program execution. The \texttt{\#\#} field is a zero-up counter that is selected to ensure that unique file names are always created, to prevent overwriting old files.

While running the simulation, it is also possible to display the images as they are computed. This can be enabled with the \texttt{--sgray} and \texttt{--scolor} options. Each image will pop up in a new window spawned by OpenCV. Press any keyboard key while one of these windows has keyboard focus in order to dismiss all of them at once.

\section{Fourier Transform Output}

One of the primary purposes of the simulation is to ``compute" the discrete Fourier transform (DFT) of the input by way of simulating wave propagation and lensing. Getting the DFT as an output file requires several additional constraints on physical dimensions and resolution parameters in order to:
\begin{enumerate}
	\item get the appropriate DFT resolution (we expect an $N \times N$ pixel input image to have an $N \times N$ DFT, despite the fact that we typically upsample the input image to a higher resolution and simulate higher output resolution than input resolution), and
	\item ensure we ``sample" the computed wave at the correct spatial resolution in order to align with the DFT bins.
\end{enumerate}

First of all, DFT output is requested with the \texttt{--oft} option. Only one DFT output file can be computed per simulation (corresponding to physical reality!). As already noted, the DFT output file will include \texttt{\_fourier\_transform} in its name. These requirements must be satisfied in order to simulate and output the DFT:

\begin{itemize}
	\item DFT bins are located at multiples of $\displaystyle \frac{\lambda f}{w_{in}}$, where $\lambda$ is set as the quotient of \texttt{--speed} and \texttt{--freq}, $f$ is the lens focal length (\texttt{--lens-flen}), and $w_{in}$ is the input width (\texttt{--inwidth}). Therefore, the output pixel size, that is, the quotient \texttt{--owidth} over \texttt{--ores}, must be exactly $\displaystyle \frac{1}{n}\frac{\lambda f}{w_{in}}$, for some integer $n \ge 1$. $n$ will be the stride at which computed wave values are sampled in order to extract DFT values.
	\item Given $n$ as defined as above, the minimum output resolution (\texttt{--ores}) is $n$ times the input resolution (prior to input upsampling, if performed). This will ensure the DFT resolution (number of bins) can match the input image resolution.
	\item To ensure alignment of the DFT and the input image along the X and Y coordinates, the difference between the output resolution (\texttt{--ores}) and minimum output resolution (defined above) must be an even number.
	\item For lens focal length (\texttt{--lens-flen}) $f$, there must be one lens located (\texttt{--lens-loc}) at $f$ from the input plane, and no other lenses within $2f$ of the input plane.
	\item There must be no filters located (\texttt{--filt-loc}) within $2f$ of the input plane.
\end{itemize}

\section{Other Simulation Parameters}
There are a number of configurable parameters supported by the simulation. Use the \texttt{--help} option to see them all. Here is a summary:
\begin{itemize}
	\item \texttt{--lens-loc}, \texttt{--lens-mode}, \texttt{--lens-rad}, \texttt{--lens-flen}, \texttt{--lens-quant}, and \texttt{--lens-data} specify lens locations, how to simulate lenses, lens radii, lens focal length (if applicable), stepped lens quantization levels (if applicable), and external file-based lens data (if applicable), respectively. The simulation will not simulate lenses unless asked. Multiple lenses at unique locations may be specified with \texttt{--lens-loc}; for example, \texttt{--lens-loc 1000,3000} places two lenses along the $z$-axis. Note that the simulation automatically knows to compute the wave at the input of each lens plane, whether or not output has been requested at those locations with the \texttt{--oloc} option. This implies that the simulation computes the wavefront at planes along the $z$-axis in sequential order, with increasing $z$.

	\item \texttt{--filt-loc} and \texttt{--filt-rad} specify filter locations and filter radii, respectively. \texttt{--filt-loc} is handled like \texttt{--lens-loc}, as described above. High-pass filters are specified by positive \texttt{--filt-rad} values, while low-pass filters are specified by negative \texttt{--filt-rad} values (with the actual radius taken as the absolute value).
	
	\item \texttt{--pressure} causes the simulation to compute waves in terms of actual pressure in Pascals. So at the input, data are mapped from voltages (normalized by $V_{dd}$) to pressure, and at the output data are mapped from pressure back to voltage (normalized by $V_{dd}$). Options related to the conversion are \texttt{--Vdd}, \texttt{--Q}, \texttt{--Y}, \texttt{--D33}, \texttt{--t}, and \texttt{--amplify}. The output may be quantized (digitized) using the \texttt{--obits} option. At this time, the ‘I’ and ‘Q’ values of the digitized data, which are just the real and imaginary parts of the digitized complex values, are not split into separate files. If \texttt{--pressure} is not asserted on the command-line, then all these other options are ignored.
\end{itemize}

\section{Parameter Files}
All the parameters for a particular run of the simulation are automatically saved in a text file named \texttt{command\_line\_\#\#.txt}. The simulation can be run later, passing this file with the \texttt{--paramfile} option, to replicate prior results (assuming no changes to the input data file or the simulation itself). Custom parameter files can also be manually created. The contents are just the exact command line one wishes to run, minus of course the executable name. Newlines and spaces may be inserted arbitrarily into the file to aid readability. Comments begin with a pound sign and continue to the end of the line.

\section{Threads}
The simulation by default will spawn a number of worker threads equal to the \emph{hardware concurrency} supported by the platform. This is typically the number of physical or virtual CPU cores. To see this number, run the program with the \texttt{--chkthreads} option. (On Linux, this is redundant to the \texttt{nproc} command.) It can be helpful to limit the number of threads used by the simulation to reserve some CPU resources for other tasks on the computer. The \texttt{--maxthreads} option can be used for this purpose.

\section{Listing of All Options}
\label{sec:option_listing}

The following listing shows the program's output in response to the \texttt{--help} option: a listing of all simulation options with brief descriptions.

\lstinputlisting[escapeinside={(*}{*)}, basicstyle=\scriptsize]{help.txt}