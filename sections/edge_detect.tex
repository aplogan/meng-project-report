\label{chapter:edge_detect}

Now that I had figured out how to use the simulation to actually compute the Fourier transforms we were after, and had shown it to be working toward this end, it was time to demonstrate an interesting application: edge detection. Edge detection is an image processing/computer vision technique in which the edges of objects in an image are highlighted (typically in white) while the remainder of the image is usually blackened, or at least muted. There are various algorithms to do this on an image, including the Canny and Sobel techniques \cite{davies_cmv_2012}. Both of these are rather involved algorithms.

A simpler, cruder approach exploits the fact that the edges in an image are conveyed by the high frequency image components, much like the sharp edges in a periodic square wave are conveyed by high frequency harmonics of the fundamental tone. So the simple edge detection approach is to merely high-pass filter a grayscale image. In the frequency domain, this can be performed crudely by merely blocking (zeroing out) lower frequency spectral components, and allowing everything else to pass.

In terms of the ultrasonic MEMS device, this high-pass filtering operation could be accomplished by means of placing some disc structure at the Fourier transform plane (plane 2) that blocks all wave vectors incident within a certain radius, and allows all others to pass. That is the approach I simulated. The simulation includes options to specify such filters, which, like lenses, are simulated with zero physical width along the $z$-axis, and perfectly block all incident wave vectors within their radius. See appendix \ref{appendix:run_sim} for more details.

The image selected for this experiment was the classic (at least, within image processing literature) cameraman image, which I reduced to $128 \times 128$ using ImageMagick; see figure \ref{fig:camera_man}. Shown with this image are the result of running Canny and Sobel edge detection, in order to provide a handy reference for the goal of the edge detection experiment.

\begin{figure}[h]
	\centering
	\subfloat[original image]{\label{fig:camera_man:a}\includegraphics[]{cameraman_128x128}}\hfill
	\subfloat[Canny edge detection]{\label{fig:camera_man:b}\includegraphics[]{cameraman_128x128_canny}}\hfill
	\subfloat[Sobel edge detection]{\label{fig:camera_man:c}\includegraphics[]{cameraman_128x128_sobel}}\\
	\caption[$128 \times 128$ cameraman image, original and edge detected]{$128 \times 128$ cameraman image, used as input to edge detection operation, along with two edge detection outputs from MATLAB, provided for reference.}
	\label{fig:camera_man}
\end{figure}

An important question is, what is an appropriate disc filter radius to do the edge detection? To answer this, I computed the 2D FFT of the image in MATLAB and applied a disc function to the result, whereby I zeroed out all FFT bins within a certain radius in terms of \emph{FFT bins}. I then computed the inverse 2D FFT. I did this for disc radii from 10 through 35 and examined the results, a subset of which is shown in figure \ref{fig:matlab_edge_detect}.

\begin{figure}[h]
	\centering
	\subfloat[$r=11$]{\label{fig:matlab_edge_detect:a}\includegraphics[]{cameraman_128x128_filt_rad_11}}\hfill
	\subfloat[$r=13$]{\label{fig:matlab_edge_detect:b}\includegraphics[]{cameraman_128x128_filt_rad_13}}\hfill
	\subfloat[$r=15$]{\label{fig:matlab_edge_detect:c}\includegraphics[]{cameraman_128x128_filt_rad_15}}\\
	\subfloat[$r=17$]{\label{fig:matlab_edge_detect:d}\includegraphics[]{cameraman_128x128_filt_rad_17}}\hfill
	\subfloat[$r=19$]{\label{fig:matlab_edge_detect:e}\includegraphics[]{cameraman_128x128_filt_rad_19}}\hfill
	\subfloat[$r=21$]{\label{fig:matlab_edge_detect:f}\includegraphics[]{cameraman_128x128_filt_rad_21}}\\
	\subfloat[$r=23$]{\label{fig:matlab_edge_detect:g}\includegraphics[]{cameraman_128x128_filt_rad_23}}\hfill
	\subfloat[$r=25$]{\label{fig:matlab_edge_detect:h}\includegraphics[]{cameraman_128x128_filt_rad_25}}\hfill
	\subfloat[$r=27$]{\label{fig:matlab_edge_detect:i}\includegraphics[]{cameraman_128x128_filt_rad_27}}\\
	\caption[$128 \times 128$ cameraman image disc high-pass filtered in frequency domain]{$128 \times 128$ cameraman image disc high-pass filtered in frequency domain in MATLAB with simple disc approach: effect of disc radius. Here, the disc radius is in terms of FFT bins. A disc of radius 19 was chosen for the edge detection operation because the result above shows the least amount of ringing.}
	\label{fig:matlab_edge_detect}
\end{figure}

A shortcoming of this approach to high-pass filtering arises from the fact that we are effectively multiplying by $1 - circ(\cdot)$ in the frequency domain. The inverse Fourier transform of $1 - circ(\cdot)$ is a delta at the origin minus the \emph{sombrero function}. The sombrero function is defined for radial coordinate $\rho$ as $somb(\rho) = 2J_1(\pi\rho)/(\pi\rho)$, where $J_1(\cdot)$ is the Bessel function of the first kind\footnote{Here, we're ignoring the details of radial scaling of these functions. Of course, radial expansion or compression of a function in one domain results in radial compression or expansion, respectively, in its Fourier transform counterpart domain.}. This function is the two-dimensional radial analog of the one-dimensional $sinc(\cdot)$ function, the inverse Fourier transform of the $rect(\cdot)$ function. A sample $circ(\cdot)$ function and its inverse Fourier transform are shown in figure \ref{fig:circ_idft}. By linearity of the Fourier transform, we can consider the effect of multiplying by $circ(\cdot)$ in the frequency domain in isolation; this frequency domain multiplication is equivalent to convolution with $somb(\cdot)$ in the spatial domain. This convolution effect appears as spatial smearing and ringing in the filtered images, and is quite apparent in figure \ref{fig:matlab_edge_detect}. Nevertheless, the edges are still highlighted in these images, so simulating this filtering operation can serve as a basic proof of concept for edge detection. Based on my subjective judgment of these images, I decided to use a filter disc radius corresponding to 19 FFT bins because this radius appears to have the \emph{least} amount of ringing while the edges are still fairly clear. Note that the contrast ratio of the display on which these images are viewed can have a significant impact on one's perception of the amount of ringing.

\begin{figure}[h]
	\centering
	\subfloat[$circ(\cdot)$ with a radius of 8 set in a $128\times 128$ image]{\label{fig:circ_idft:a}\includegraphics[width=0.49\textwidth]{circ_idft_a}}\hfill
	\subfloat[Absolute value of inverse DFT of (a)]{\label{fig:circ_idft:b}\includegraphics[width=0.49\textwidth]{circ_idft_b}}\\
	\caption{$circ(\cdot)$ and its inverse Fourier transform, $somb(\cdot)$}
	\label{fig:circ_idft}
\end{figure}

Next, I proceeded to run the edge detection simulation using the parameters given in table \ref{table:edge_detect_parameters}.
\begin{table}[h]
	\centering
	\begin{tabular}{|p{0.4\textwidth}|p{0.4\textwidth}|}
		\hline
		\textbf{Parameter} & \textbf{Value} \\
		\hline
		Input image resolution & $128 \times 128$ pixels \\
		\hspace{1em}(before upsampling) & \\
		\hspace{1em}($N_{trans} \times N_{trans}$) & \\
		Input image upsampling & $15\times$ \\
		Input resolution ($N_{in} \times N_{in}$)& $1920 \times 1920$ pixels \\
		\hspace{1em}(after upsampling) & \\
		Input width ($w_{in}$) & $6400\ \mu m$ \\
		Transducer size & $(6400\ \mu m) / 128 = 50\ \mu m$ \\
		Input pixel size & $(6400\ \mu m) / 1920 \approx 3.33\ \mu m$ \\
		Wavelength & $(8433\ m/s) / (1.2\ GHz) = 7.0275\ \mu m$ \\
		Input pixels per wavelength & $(7.0275\ \mu m) / (3.33\ \mu m) \approx 2.11$ \\
		Output width ($w_{out}$) & $12804.105\ \mu m \approx 2w_{in}$ \\
		Output resolution  ($N_{out} \times N_{out}$) & $3644 \times 3644$ pixels \\
		Output pixel size & $(12804.105\ \mu m) / 3644 = 3.51375\ \mu m$ \\
		Output pixels per wavelength & $(7.0275\ \mu m) / (3.51375\ \mu m) = 2$ \\
		Lens focal length ($f$) & $6400\ \mu m$ \\
		Lens radius ($r_{lens}$) & $w_{out}/2 = 6402.0525\ \mu m$ \\
		Planes 1-4 $z$-axis locations & $6400,12800,19200,25600\ \mu m$ \\
		Location of first Fourier & $\lambda f / w_{in} = 7.0275\ \mu m$ \\
		\hspace{1em}transform bin in X or Y & \\
		Radius of filter disc & $19 \cdot 7.0275\ \mu m = 133.5225\ \mu m$ \\
		& \hspace{1em}(19 Fourier transform bins) \\
		\hline
	\end{tabular}
	\caption{Simulation parameters for the edge detection experiment}
	\label{table:edge_detect_parameters}
\end{table}

Simulation output was computed at planes 1-4 and was saved as grayscale and colormapped images, and CSV files of the raw data. The Fourier transform at plane 2, prior to filtering, was also extracted and output in these three formats.

The simulation was run on a Windows 10 machine with 16 physical cores (32 logical cores) nominally clocked at 2.80 GHz, and 128 GB of RAM. The simulation was run with 30 worker threads. Although the \emph{hardware concurrency} of the machine supports 32 parallel worker threads, only 30 were used in order to keep the machine responsive to interactive tasks (e.g. viewing output images as they were created). The total elapsed time to run the simulation was nearly \textbf{351 hours (more than two weeks)}, although this includes several hours of downtime when the simulation was paused when others were using the same computer.

To better appreciate where all this run-time went, examine again equation \ref{eq:cplx_integrand}. We can see that this expression requires computation of a sine and a cosine, which dominate the run-time. For the plane 0 to plane 1 integration, this integrand must be computed $N_{in}^2$ times, for each of $N_{out}^2$ points. For the plane 1 to plane 2, plane 1 to plane 3, and plane 3 to plane 4 integrations, this integrand must be computed $N_{out}^2$ times, for each of $N_{out}^2$ points. Putting this all together, this is $(N_{in}^2+3N_{out}^2)N_{out}^2$ total calculations of equation \ref{eq:cplx_integrand}. For $N_{in} = 1920$ and $N_{out} = 3644$, this comes out to $(1920^2 + 3\cdot3644^2) \cdot 3644^2 \approx 578\mathrm{e}{+12}$. So, considering equation \ref{eq:cplx_integrand} had to be computed 578 trillion times, it's no wonder the simulation took so long to run to completion!

The results of this experiment are shown in figures \ref{fig:edge_detect_results} and \ref{fig:edge_detect_results_high_res}. In particular, the final output image (plane 4) is not what we expected -- compare with figure \ref{fig:matlab_edge_detect:e}.

\begin{figure}[h]
	\centering
	\subfloat[Plane 1, post lens]{\label{fig:edge_detect_results:a}\includegraphics[width=0.32\textwidth]{cameraman_128x128_6400_um_gray}}\hfill
	\subfloat[Plane 2, pre filter]{\label{fig:edge_detect_results:b}\includegraphics[width=0.32\textwidth]{cameraman_128x128_12800_um_gray}}\hfill
	\subfloat[Plane 2, post filter]{\label{fig:edge_detect_results:c}\includegraphics[width=0.32\textwidth]{cameraman_128x128_12800_um_post_filt_gray}}\\
	\subfloat[Plane 3, pre lens]{\label{fig:edge_detect_results:d}\includegraphics[width=0.32\textwidth]{cameraman_128x128_19200_um_gray}}\hfill
	\subfloat[Plane 3, post lens]{\label{fig:edge_detect_results:e}\includegraphics[width=0.32\textwidth]{cameraman_128x128_19200_um_post_lens_gray}}\hfill
	\subfloat[Plane 4]{\label{fig:edge_detect_results:f}\includegraphics[width=0.32\textwidth]{cameraman_128x128_25600_um_gray}}\\
	\caption[Various stages of edge detection simulation]{Various stages of edge detection simulation. The plane 1 pre- and post-lens images appear identical; hence only the latter is shown. Images are scaled from $3644 \times 3644$ pixels.}
	\label{fig:edge_detect_results}
\end{figure}

In the high-resolution plane 4 image in figure \ref{fig:edge_detect_results_high_res}, observe a few phenomena:

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{cameraman_128x128_25600_um_gray}
	\caption[Plane 4 output of edge detection experiment]{Plane 4 output of edge detection experiment; this is a larger scaled version of figure \ref{fig:edge_detect_results:f}. Image is scaled from $3644 \times 3644$ pixels.}
	\label{fig:edge_detect_results_high_res}
\end{figure}

\begin{itemize}
	\item The image is upside down relative to the input image, as expected for the two-lens system of figure \ref{fig:planes}.
	\item The illuminated portion of the image fills half the field of view and is overlaid on a black background. With $w_{in} = 6400\ \mu m$ and $w_{out} \approx 12800\ \mu m$, the illuminated portion matches the input image size.
	\item The corers are slightly truncated, as a result of the lens aperture being simulated with insufficiently large radius $r_{lens}$. The source of this truncation is immediately apparent by comparing figures \ref{fig:edge_detect_results:d} and \ref{fig:edge_detect_results:e}. Perhaps a lens radius $\sqrt{2} \approx 1.41$ times larger would have prevented this effect, but that would have required the output images to be correspondingly larger, resulting in roughly $(\sqrt{2})^4 = 4$ times longer simulation run-time for the same pixel size. Remember that this simulation took more than two weeks to run with the available computing resources, so this small amount of truncation is an acceptable trade-off.
	\item Most notably, the illuminated portion of the plane 4 image is not what we expect. At the center of the image, we see a filtered image that more or less matches what was predicted in image \ref{fig:matlab_edge_detect:e}. But at a radius of about 2/3 of $w_{in}/2$ (about $2100\ \mu m$) we see the filtered image transition to the unfiltered input image. So a portion of the expected edge detected image is vignetted within the original input image itself.
	\item There is a strange, outward-radiating circular ripple effect in the image. It is unclear if this is related or not to the vignetting effect.
\end{itemize}

What's going on here? In an effort to debug these results, I first tried computing in MATLAB the inverse discrete Fourier transforms of the plane 2 images, both pre- and post-filtering (figures \ref{fig:edge_detect_results:b} and \ref{fig:edge_detect_results:c}). I did this for both the $3644 \times 3644$ resolution at which the simulation operated, as well as the $128 \times 128$ images created by sub-sampling those full-resolution images for Fourier transform resolution. The results are shown in figure \ref{fig:matlab_ifft}.

\begin{figure}[h]
	\centering
	\subfloat[Wave at plane 2, pre filter]{\label{fig:matlab_ifft:a}\includegraphics[width=0.235\textwidth]{matlab_ifft_a}}\hfill
	\subfloat[Wave at plane 2, post filter]{\label{fig:matlab_ifft:b}\includegraphics[width=0.235\textwidth]{matlab_ifft_c}}\hfill
	\subfloat[Fourier Transform extracted (in simulation) from (a)]{\label{fig:matlab_ifft:c}\includegraphics[width=0.235\textwidth]{matlab_ifft_b}}\hfill
	\subfloat[Fourier Transform extracted (in MATLAB) from (b)]{\label{fig:matlab_ifft:d}\includegraphics[width=0.235\textwidth]{matlab_ifft_d}}\\
	\subfloat[MATLAB inverse DFT of (a)]{\label{fig:matlab_ifft:e}\includegraphics[width=0.235\textwidth]{matlab_ifft_a_ifft}}\hfill
	\subfloat[MATLAB inverse DFT of (b)]{\label{fig:matlab_ifft:f}\includegraphics[width=0.235\textwidth]{matlab_ifft_c_ifft}}\hfill
	\subfloat[MATLAB inverse DFT of (c)]{\label{fig:matlab_ifft:g}\includegraphics[width=0.235\textwidth]{matlab_ifft_b_ifft}}\hfill
	\subfloat[MATLAB inverse DFT of (d)]{\label{fig:matlab_ifft:h}\includegraphics[width=0.235\textwidth]{matlab_ifft_d_ifft}}\\
	\caption[Computation of inverse DFT of various edge detection simulation outputs]{Computation of inverse discrete Fourier transform in MATLAB of various edge detection simulation outputs. Images (a), (b), (e), and (f) are scaled from $3644 \times 3644$ pixels. Images (c), (d), (g), and (h) are scaled from $128 \times 128$ pixels.}
	\label{fig:matlab_ifft}
\end{figure}

Amazingly, the full-resolution inverse Fourier transform images (figures \ref{fig:matlab_ifft:e} and \ref{fig:matlab_ifft:f}) very closely match the input image and plane 4 simulation output image, respectively. This confirms that the simulation's computations for planes 3 and 4, given the plane 2 image, are correct. This result is also confirmed, qualitatively, by the low-resolution inverse Fourier transform images (figures \ref{fig:matlab_ifft:e} and \ref{fig:matlab_ifft:f}), although these images suffer from apparent numerical error effects that manifest near the outer image boundaries.

Next, I was curious if I might somehow have picked an inappropriate filter radius. So starting with the unfiltered plane 2 image (figure \ref{fig:edge_detect_results:b}), I applied a disc filter in MATLAB and then computed the inverse Fourier transform the result. The result of doing this with a variety of filter radii is shown in figure \ref{fig:a_filt_ifft}. The results of a similar experiment, except starting with the plane 2 image at Fourier transform resolution (figure \ref{fig:matlab_ifft:c}) and using different filter radii, is shown in figure \ref{fig:b_filt_ifft}. Unfortunately, in none of these results was I able to make the vignetting effect go away.

\begin{figure}[h]
	\centering
	\subfloat[$r=  5$]{\label{a_filt_ifft:a}\includegraphics[width=0.235\textwidth]{a_filt_05px}}\hfill
	\subfloat[$r= 20$]{\label{a_filt_ifft:b}\includegraphics[width=0.235\textwidth]{a_filt_20px}}\hfill
	\subfloat[$r= 35$]{\label{a_filt_ifft:c}\includegraphics[width=0.235\textwidth]{a_filt_35px}}\hfill
	\subfloat[$r= 50$]{\label{a_filt_ifft:d}\includegraphics[width=0.235\textwidth]{a_filt_50px}}\\
	\subfloat[$r= 65$]{\label{a_filt_ifft:e}\includegraphics[width=0.235\textwidth]{a_filt_65px}}\hfill
	\subfloat[$r= 80$]{\label{a_filt_ifft:f}\includegraphics[width=0.235\textwidth]{a_filt_80px}}\hfill
	\subfloat[$r= 95$]{\label{a_filt_ifft:g}\includegraphics[width=0.235\textwidth]{a_filt_95px}}\hfill
	\subfloat[$r=110$]{\label{a_filt_ifft:h}\includegraphics[width=0.235\textwidth]{a_filt_110px}}\\
	\subfloat[$r=125$]{\label{a_filt_ifft:i}\includegraphics[width=0.235\textwidth]{a_filt_125px}}\hfill
	\subfloat[$r=140$]{\label{a_filt_ifft:j}\includegraphics[width=0.235\textwidth]{a_filt_140px}}\hfill
	\subfloat[$r=155$]{\label{a_filt_ifft:k}\includegraphics[width=0.235\textwidth]{a_filt_155px}}\hfill
	\subfloat[$r=170$]{\label{a_filt_ifft:l}\includegraphics[width=0.235\textwidth]{a_filt_170px}}\\
	\caption[Result of disc filtering the plane 2 image and computing the inverse DFT]{Result of disc filtering the plane 2 image in figure \ref{fig:edge_detect_results:b} and computing the inverse DFT in MATLAB. The labels are the disc filter radius in pixels. Images are scaled from $3644 \times 3644$ pixels.}
	\label{fig:a_filt_ifft}
\end{figure}

\begin{figure}[h]
	\centering
	\subfloat[$r= 2$]{\label{fig:b_filt_ifft:a}\includegraphics[width=0.235\textwidth]{b_filt_02px}}\hfill
	\subfloat[$r= 4$]{\label{fig:b_filt_ifft:b}\includegraphics[width=0.235\textwidth]{b_filt_04px}}\hfill
	\subfloat[$r= 6$]{\label{fig:b_filt_ifft:c}\includegraphics[width=0.235\textwidth]{b_filt_06px}}\hfill
	\subfloat[$r= 8$]{\label{fig:b_filt_ifft:d}\includegraphics[width=0.235\textwidth]{b_filt_08px}}\\
	\subfloat[$r=10$]{\label{fig:b_filt_ifft:e}\includegraphics[width=0.235\textwidth]{b_filt_10px}}\hfill
	\subfloat[$r=12$]{\label{fig:b_filt_ifft:f}\includegraphics[width=0.235\textwidth]{b_filt_12px}}\hfill
	\subfloat[$r=14$]{\label{fig:b_filt_ifft:g}\includegraphics[width=0.235\textwidth]{b_filt_14px}}\hfill
	\subfloat[$r=16$]{\label{fig:b_filt_ifft:h}\includegraphics[width=0.235\textwidth]{b_filt_16px}}\\
	\subfloat[$r=18$]{\label{fig:b_filt_ifft:i}\includegraphics[width=0.235\textwidth]{b_filt_18px}}\hfill
	\subfloat[$r=20$]{\label{fig:b_filt_ifft:j}\includegraphics[width=0.235\textwidth]{b_filt_20px}}\hfill
	\subfloat[$r=22$]{\label{fig:b_filt_ifft:k}\includegraphics[width=0.235\textwidth]{b_filt_22px}}\hfill
	\subfloat[$r=24$]{\label{fig:b_filt_ifft:l}\includegraphics[width=0.235\textwidth]{b_filt_24px}}\\
	\caption[Result of disc filtering the plane 2 FT image and computing the inverse DFT]{Result of disc filtering the plane 2 Fourier Transform image in figure \ref{fig:matlab_ifft:c} and computing the inverse DFT in MATLAB. The labels are the disc filter radius in pixels. Images are scaled from $128 \times 3128$ pixels.}
	\label{fig:b_filt_ifft}
\end{figure}

\clearpage
Finally, out of curiosity, I re-ran the simulation for planes 3 and 4, except without filtering at plane 2. The results of this experiment are shown in figure \ref{fig:no_edge_detect_results}. It is comforting to see the plane 4 image very closely matches the input image, except inverted and with truncated corners as previously observed for figure \ref{fig:edge_detect_results_high_res}. These results give us strong confirmation that the simulation is indeed working as expected, at least for the no-filtering case.

\begin{figure}[h]
	\centering
	\subfloat[Plane 3, pre lens]{\label{no_edge_detect:a}\includegraphics[width=0.32\textwidth]{cameraman_128x128_12800_um_6400_um_gray}}\hfill
	\subfloat[Plane 3, post lens]{\label{no_edge_detect:b}\includegraphics[width=0.32\textwidth]{cameraman_128x128_12800_um_6400_um_post_lens_gray}}\hfill
	\subfloat[Plane 4]{\label{no_edge_detect:c}\includegraphics[width=0.32\textwidth]{cameraman_128x128_12800_um_12800_um_gray}}\\
	\caption[Alternative simulation of planes 3 and 4 without filtering at plane 2]{Alternative simulation of planes 3 and 4 for which no filtering takes place at plane 2. Images are scaled from $3644 \times 3644$ pixels.}
	\label{fig:no_edge_detect_results}
\end{figure}

So far, my investigations did not yield any useful insight into what was giving the unexpected results of figure \ref{fig:edge_detect_results_high_res}. It wasn't until I worked through writing the background material of section \ref{sec:fourier_optics} that I was able to develop a hunch. The simulation only explicitly computes wave propagation via equation \ref{eq:bg4} and lens transformations via equation \ref{eq:bg5}. Neither equation's derivation relied on approximations; therefore the only approximations inherent to the simulation's computations are the fact that it must use finite image resolution and that it must perform numerical integration. However, the derivation of the equation \ref{eq:bg8} Fourier transform relation \emph{does} rely on approximations, and as detailed in that section, its derivation relies on the assumption of large inter-plane spacing, such that for any two points in the source and observation planes, $\cos(\mathbf{\bar{n}},\mathbf{\bar{r}_{0,1}}) \approx 1$.

In this light, consider the following source- and observation-plane coordinates, $(x_1,y_1,z_1)$ and $(x_0,y_0,z_0)$, for the plane 1 to plane 2 application of equation \ref{eq:bg4}:
\begin{itemize}
	\item The top-left corner of the illuminated portion of figure \ref{fig:edge_detect_results:a}:
	\newline$(x_1,y_1,z_1) \approx \left(-\frac{w_{in}}{2},-\frac{w_{in}}{2},f \right) = (-3200,-3200,6400)\ \mu m$
	\item The outer-most Fourier transform bin in the plane 2 first quadrant:
	\newline$(x_0,y_0,z_0) \approx \left( \frac{N_{FT}}{2}\frac{\lambda f}{w_{in}},\frac{N_{FT}}{2}\frac{\lambda f}{w_{in}},2f \right) \approx (450,450,12800)\ \mu m$
\end{itemize}

Now plug these coordinates into equation \ref{eq:bg3}:

\begin{equation*}
	\begin{split}
	\cos(\mathbf{\bar{n}},\mathbf{\bar{r}_{0,1}})
	&= \frac{z_0-z_1}{r_{0,1}} \\
	&= \frac{z_0-z_1}{\sqrt{(x_0-x_1)^2 + (y_0-y_1)^2 + (z_0-z_1)^2}} \\
	&= \frac{12800-6400}{\sqrt{(450-(-3200))^2 + (450-(-3200))^2 + (12800-6400)^2}} \\
	&\approx 0.78 \\
	&\not\approx 1
	\end{split}
\end{equation*}

So clearly, given the simulation parameters of table \ref{table:edge_detect_parameters}, the condition of $\cos(\mathbf{\bar{n}},\mathbf{\bar{r}_{0,1}}) \approx 1$ is not satisfied, and therefore we can't safely assume that the Fourier transform relation, equation \ref{eq:bg8}, holds.

With this said, I am unable to account for why computing the inverse Fourier transform in MATLAB -- which, unlike doing so in the simulation, is ``exact", insofar as it can be in MATLAB -- of the plane 2 images produced results that so closely matched the simulation output. That is, if the conditions necessary for the plane 2 image to represent the Fourier transform of the input image do not hold, then I don't understand why computing the inverse Fourier transform in MATLAB, produced results that so closely matched what the simulation computed, as shown in figure \ref{fig:matlab_ifft}.

At any rate, identifying the cause of the edge detection experiment's failure to meet my expectations leads to a simple solution: increase the lens focal length $f$. I propose rerunning the edge detection simulation with new focal length $f' = 10f = 64\ mm$. With this new focal length, the two coordinates previously considered are now:

\begin{itemize}
	\item The top-left corner of the illuminated portion of figure \ref{fig:edge_detect_results:a}:
	\newline$(x_1,y_1,z'_1) \approx (-\frac{w_{in}}{2},-\frac{w_{in}}{2},f') = (-3.2,-3.2,64)\ mm$
	\item The outer-most Fourier transform bin in the plane 2 first quadrant:
	\newline$(x'_0,y'_0,z'_0) \approx (\frac{N_{FT}}{2}\frac{\lambda f'}{w_{in}},\frac{N_{FT}}{2}\frac{\lambda f'}{w_{in}},2f') \approx (4.5,4.5,128)\ mm$
\end{itemize}

And the evaluation of equation \ref{eq:bg3} becomes:

\begin{equation*}
	\cos(\mathbf{\bar{n}},\mathbf{\bar{r}_{0,1}})
	= \frac{128-64}{\sqrt{(4.5-(-3.2))^2 + (4.5-(-3.2))^2 + (128-64)^2}}
	\approx 0.99
	\approx 1
\end{equation*}

Therefore, using a longer lens focal length in the simulation seems to provide the conditions necessary for the Fourier transform relation, equation \ref{eq:bg8}, to be valid. I think repeating the edge detection experiment using all the parameters of table \ref{table:edge_detect_parameters}, except with focal length $f' = 64\ mm$, might yield something closer to the desired results.